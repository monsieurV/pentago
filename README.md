# README #


### What is this repository for? ###

* This is a simple pentago game make with pygame.

### How do I get set up? ###

* You need a working installation of python3 pygame library.
* You can change the file config.py in order to configure the game (no menu for now) :
    - "FRAME_SIZE" -> Number of frame of the side of a quadrant
    - "GAME_SIZE" -> Number of quadrant of a side of the game board
    - "WIN_MIN_LENGTH_COND" -> The minimum line lenght for the win condition
    - "PLAYERS" -> Use to specified if players are Human or COMPUTER (for example : Replace HUMAN by COMPUTER in order to play with an AI)
    - All the remaining configuration is used for the interface. No need to change it.
* Exiting the application during a game will automatically save it and reload it next time you start the softward. (Be carefull, Loading a save will not work correctly if you change the configuration. In order to avoid this, don't forget to remove save file before restarting the program if you change the configuration file)