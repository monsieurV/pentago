import pygame

# CONSTANTES DEFINITION
NONE            = 0
PUT_PAWN        = 1
ROTATE_FRAME    = 2
AI_PLAYING      = 3
HUMAN           = 1
COMPUTER        = 2

CONFIG = {
    # GAME RULES
    'FRAME_SIZE'        : 3,
    'GAME_SIZE'         : 2,
    'WIN_MIN_LENGTH_COND' : 5,
    'PLAYERS'           : {1: HUMAN, 2: COMPUTER},
    # DISPLAY CONFIGURATIONS
    'WINDOW_TITLE'      : 'pentagoGui',
    'SQUARE_WIDTH'      : 75,
    'MARGIN'            : 5,
    'MIN_HEIGHT'        : 420,
    'BOARD_POS_X'       : 5,
    'AI_SLEEP_TIME'     : 1,
    'BACKGROUND_COLOR'  : (235, 176, 100),
    'LINE_COLOR'        : (0, 68, 126),
    'SAVE_FILE_NAME'    : 'save'
}

IMAGES = {
    'FRAME'         : "images/frame.png",
    'FRAME_BORDER'  : "images/frame_border.png",
    'SQUARE'        : "images/square.png",
    'PLAYER1'       : "images/player_1.png",
    'PLAYER2'       : "images/player_2.png",
    'COMPUTER'      : "images/computer.png",
    'PUTPAWN'       : "images/put_pawn.png",
    'ROTATEFRAME'   : "images/rotate_frame.png",
    'WINNER'        : "images/winner.png",
    'RESET'         : "images/reset.png"
}

SOUNDS = {
    'PUTPAWN'       : "sounds/put_pawn.wav",
    'ROTATEFRAME'   : "sounds/rotate_frame.wav",
    'VICTORY'       : "sounds/victory.wav",
}

# DERIVED CONFIGURATION
CONFIG['SQUARE_TEXTURE_WIDTH']  = 0
CONFIG['FRAME_WIDTH']           = 0
CONFIG['FRAME_QUANTITY']        = 0
CONFIG['GAMEBOARD_SIZE']        = 0
CONFIG['GAME_WIDTH']            = 0
CONFIG['WINDOW_HEIGHT']         = 0
CONFIG['WINDOW_WIDTH']          = 0
CONFIG['BOARD_POS_Y']           = 0
IMG_RESOURCES                   = {}
SOUND_RESOURCES                 = {}

def updateConfiguration():
    CONFIG['SQUARE_TEXTURE_WIDTH']  = CONFIG['SQUARE_WIDTH']//2
    CONFIG['FRAME_WIDTH']           = CONFIG['SQUARE_WIDTH'] * CONFIG['FRAME_SIZE']
    CONFIG['FRAME_QUANTITY']        = CONFIG['GAME_SIZE'] * CONFIG['GAME_SIZE']
    CONFIG['GAMEBOARD_SIZE']        = CONFIG['GAME_SIZE'] * CONFIG['FRAME_SIZE']
    CONFIG['GAME_WIDTH']            = CONFIG['GAME_SIZE'] * CONFIG['FRAME_WIDTH']
    CONFIG['WINDOW_HEIGHT']         = CONFIG['GAME_WIDTH'] + 10 if CONFIG['GAME_WIDTH'] > CONFIG['MIN_HEIGHT'] else CONFIG['MIN_HEIGHT'] + 10
    CONFIG['WINDOW_WIDTH']          = CONFIG['GAME_WIDTH'] + 128
    CONFIG['BOARD_POS_X']           = CONFIG['MARGIN']
    CONFIG['BOARD_POS_Y']           = (CONFIG['WINDOW_HEIGHT'] - CONFIG['GAME_WIDTH']) // 2

def loadRessources():
    IMG_RESOURCES['FRAME']          = pygame.image.load(IMAGES['FRAME']).convert_alpha()
    IMG_RESOURCES['FRAME']          = pygame.transform.scale(IMG_RESOURCES['FRAME'], (CONFIG['FRAME_WIDTH'], CONFIG['FRAME_WIDTH']))
    IMG_RESOURCES['FRAME_BORDER']   = pygame.image.load(IMAGES['FRAME_BORDER']).convert_alpha()
    IMG_RESOURCES['FRAME_BORDER']   = pygame.transform.scale(IMG_RESOURCES['FRAME_BORDER'], (CONFIG['FRAME_WIDTH'], CONFIG['FRAME_WIDTH']))
    IMG_RESOURCES['SQUARE']         = pygame.image.load(IMAGES['SQUARE']).convert_alpha()
    IMG_RESOURCES['PLAYER1']        = pygame.image.load(IMAGES['PLAYER1']).convert_alpha()
    IMG_RESOURCES['PLAYER2']        = pygame.image.load(IMAGES['PLAYER2']).convert_alpha()
    IMG_RESOURCES['COMPUTER']       = pygame.image.load(IMAGES['COMPUTER']).convert_alpha()
    IMG_RESOURCES['PUTPAWN']        = pygame.image.load(IMAGES['PUTPAWN']).convert_alpha()
    IMG_RESOURCES['ROTATEFRAME']    = pygame.image.load(IMAGES['ROTATEFRAME']).convert_alpha()
    IMG_RESOURCES['WINNER']         = pygame.image.load(IMAGES['WINNER']).convert_alpha()
    IMG_RESOURCES['RESET']          = pygame.image.load(IMAGES['RESET']).convert_alpha()

    SOUND_RESOURCES['PUTPAWN']      = pygame.mixer.Sound("sounds/put_pawn.wav")
    SOUND_RESOURCES['ROTATEFRAME']  = pygame.mixer.Sound("sounds/rotate_frame.wav")
    SOUND_RESOURCES['VICTORY']      = pygame.mixer.Sound("sounds/victory.wav")
