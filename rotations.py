from math import ceil
from bases import get_frame_board_pos

def rotate_board_clockwise(myList, m, start=(0,0)):
    lineMiddle, lineEnd = ceil(m/2), m-1
    endPos = (start[0] + m-1, start[1] + m-1) 

    for i in range(lineMiddle):
        for j in range(i, lineEnd-i):
            myList[start[0]+i][start[1]+j], myList[endPos[0]-j][start[1]+i], \
                myList[endPos[0]-i][endPos[1]-j], myList[start[0]+j][endPos[1]-i] \
                = myList[endPos[0]-j][start[1]+i], myList[endPos[0]-i][endPos[1]-j], \
                myList[start[0]+j][endPos[1]-i], myList[start[0]+i][start[1]+j]

def rotate_board_counter_clockwise(myList, m, start=(0,0)):
    lineMiddle, lineEnd = ceil(m/2), m-1
    endPos = (start[0] + m-1, start[1] + m-1)

    for i in range(lineMiddle):
        for j in range(i, lineEnd-i):
            myList[start[0]+i][start[1]+j], myList[start[0]+j][endPos[1]-i], \
                myList[endPos[0]-i][endPos[1]-j], myList[endPos[0]-j][start[1]+i] \
                = myList[start[0]+j][endPos[1]-i], myList[endPos[0]-i][endPos[1]-j], \
                myList[endPos[0]-j][start[1]+i], myList[start[0]+i][start[1]+j]

def rotate_board(myList, n, frameNumber, gameSize, clockwise=True):
    frameSize = n // gameSize
    pos = get_frame_board_pos(frameNumber, frameSize, gameSize)
    
    if not pos:
        return

    if clockwise:
        rotate_board_clockwise(myList, frameSize, start=pos)
    else:
        rotate_board_counter_clockwise(myList, frameSize, start=pos)
