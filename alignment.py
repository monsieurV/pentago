from bases import *

def horizontal_alignement(myList, n, minSize, value):
    for i in range(n):
        alignmentSize = 0
        for j in range(n):
            alignmentSize = alignmentSize + 1 if myList[i][j] == value else 0

            if alignmentSize == minSize:
                return True

    return False          

def vertical_alignement(myList, n, minSize, value):
    for j in range(n):
        alignmentSize = 0
        for i in range(n):
            alignmentSize = alignmentSize + 1 if myList[i][j] == value else 0

            if alignmentSize == minSize:
                return True

    return False

def diagonal_TL_BR_alignment(myList, n, minSize, value):
    for x in range(n):
        alignmentSize1, previousValue1 = 0, 0
        alignmentSize2, previousValue2 = 0, 0
        for i in range(n-x):
            alignmentSize1 = alignmentSize1 + 1 if myList[x+i][i] == value else 0
            alignmentSize2 = alignmentSize2 + 1 if myList[i][x+i] == value else 0

            if alignmentSize1 == minSize or alignmentSize2 == minSize:
                return True

    return False

def diagonal_TR_BL_alignment(myList, n, minSize, value):
    for x in range(n):
        alignmentSize1, previousValue1 = 0, 0
        alignmentSize2, previousValue2 = 0, 0
        for i in range(n-x):
            alignmentSize1 = alignmentSize1 + 1 if myList[n-x-i-1][i] == value else 0
            alignmentSize2 = alignmentSize2 + 1 if myList[n-i-1][x+i] == value else 0

            if alignmentSize1 == minSize or alignmentSize2 == minSize:
                return True

    return False

def alignment(myList, n, minSize, value):
    return horizontal_alignement(myList, n, minSize, value) or vertical_alignement(myList, n, minSize, value) \
        or diagonal_TL_BR_alignment(myList, n, minSize, value) or diagonal_TR_BL_alignment(myList, n, minSize, value)
