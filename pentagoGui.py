import pygame, sys, os, random, time
from pygame.locals import *
from bases import *
from rotations import *
from alignment import *
from config import *

def draw_board(gameBoard, gameSurface):
    pygame.draw.rect(gameSurface, CONFIG['BACKGROUND_COLOR'], (0, 0, CONFIG['GAME_WIDTH'], CONFIG['GAME_WIDTH']))
    for i in range(CONFIG['FRAME_QUANTITY']):
        frameBoardPos = get_frame_board_pos(i+1, CONFIG['FRAME_SIZE'], CONFIG['GAME_SIZE'])
        framePos = get_frame_pos(i+1, CONFIG['GAME_SIZE'])

        gameSurface.blit(get_frame_surface(gameBoard, frameBoardPos), (framePos[1] * CONFIG['FRAME_WIDTH'], framePos[0] * CONFIG['FRAME_WIDTH']))

def get_frame_surface(gameBoard, boardPos):
    frameSurface = pygame.Surface((CONFIG['FRAME_WIDTH'], CONFIG['FRAME_WIDTH']), pygame.SRCALPHA, 32).convert_alpha()
    frameSurface.blit(IMG_RESOURCES['FRAME'], (0,0))

    for i in range(CONFIG['FRAME_SIZE']):
        pygame.draw.line(frameSurface, CONFIG['LINE_COLOR'], (i*CONFIG['SQUARE_WIDTH'] + (CONFIG['SQUARE_WIDTH'])//2, (CONFIG['SQUARE_WIDTH'])//2), \
            (i*CONFIG['SQUARE_WIDTH'] + (CONFIG['SQUARE_WIDTH'])//2, CONFIG['FRAME_WIDTH'] - ((CONFIG['SQUARE_WIDTH'])//2)), 5)
        pygame.draw.line(frameSurface, CONFIG['LINE_COLOR'], ((CONFIG['SQUARE_WIDTH'])//2, i*CONFIG['SQUARE_WIDTH'] + (CONFIG['SQUARE_WIDTH'])//2), \
            (CONFIG['FRAME_WIDTH'] - ((CONFIG['SQUARE_WIDTH'])//2), i*CONFIG['SQUARE_WIDTH'] + (CONFIG['SQUARE_WIDTH'])//2), 5)

    for i in range(CONFIG['FRAME_SIZE']):
        for j in range(CONFIG['FRAME_SIZE']):
            value = gameBoard[boardPos[0]+i][boardPos[1]+j]
        
            if value == 1:
                surface = IMG_RESOURCES['PLAYER1']
            elif value == 2:
                surface = IMG_RESOURCES['PLAYER2']
            else:
                surface = IMG_RESOURCES['SQUARE']
            
            surface = pygame.transform.scale(surface, (CONFIG['SQUARE_TEXTURE_WIDTH'], CONFIG['SQUARE_TEXTURE_WIDTH']))
            frameSurface.blit(surface, (j*CONFIG['SQUARE_WIDTH'] + (CONFIG['SQUARE_WIDTH']-CONFIG['SQUARE_TEXTURE_WIDTH'])//2, i*CONFIG['SQUARE_WIDTH'] + (CONFIG['SQUARE_WIDTH']-CONFIG['SQUARE_TEXTURE_WIDTH'])//2))
               

    return frameSurface

def in_game_board(coord):
    return coord[0] > CONFIG['BOARD_POS_X'] and coord[0] < CONFIG['BOARD_POS_X'] + CONFIG['GAME_WIDTH'] and coord[1] > CONFIG['BOARD_POS_Y'] and coord[1] < CONFIG['BOARD_POS_Y'] + CONFIG['GAME_WIDTH']

def get_frame_number_from_coordinate(coord):
    if in_game_board(coord):
        x = (coord[0] - CONFIG['BOARD_POS_X']) // CONFIG['FRAME_WIDTH']
        y = (coord[1] - CONFIG['BOARD_POS_Y']) // CONFIG['FRAME_WIDTH']

        return y * CONFIG['GAME_SIZE'] + x + 1

    return False

def get_coordinate_from_frame_number(FrameNumber):
    if FrameNumber > 0 and FrameNumber <= CONFIG['FRAME_QUANTITY']:
        FrameNumber = FrameNumber - 1
        x = (FrameNumber % CONFIG['GAME_SIZE']) * CONFIG['FRAME_WIDTH'] + CONFIG['BOARD_POS_X']
        y = (FrameNumber // CONFIG['GAME_SIZE']) * CONFIG['FRAME_WIDTH'] + CONFIG['BOARD_POS_Y']

        return (x, y)

    return False

def get_board_pos_from_coordinate(coord):
    if in_game_board(coord):
        x = (coord[1] - CONFIG['BOARD_POS_Y']) // CONFIG['SQUARE_WIDTH']
        y = (coord[0] - CONFIG['BOARD_POS_X']) // CONFIG['SQUARE_WIDTH']

        return (x, y)

    return False

def get_coordinate_from_board_pos(pos):
    if pos[0] >= 0 and pos[0] < CONFIG['GAMEBOARD_SIZE'] and pos[1] >= 0 and pos[1] < CONFIG['GAMEBOARD_SIZE']:
        x = pos[1] * CONFIG['SQUARE_WIDTH'] + CONFIG['BOARD_POS_Y']
        y = pos[0] * CONFIG['SQUARE_WIDTH'] + CONFIG['BOARD_POS_X']

        return (x, y)

    return False

def put_pawn(gameBoard, playerNumber, pos):
    if gameBoard[pos[0]][pos[1]] == 0:
        gameBoard[pos[0]][pos[1]] = playerNumber
        SOUND_RESOURCES['PUTPAWN'].play()

        return True

    return False

def put_random_pawn(gameBoard, playerNumber):
    time.sleep(CONFIG['AI_SLEEP_TIME'])
    if empty_square_remaining(gameBoard):
        played = False
        x =  random.randrange(0, CONFIG['GAMEBOARD_SIZE'])
        y =  random.randrange(0, CONFIG['GAMEBOARD_SIZE'])

        while gameBoard[x][y] != 0:
            x = x + 1
            if x >= CONFIG['GAMEBOARD_SIZE']:
                x =0
                y = y + 1 if y + 1 < CONFIG['GAMEBOARD_SIZE'] else 0

        put_pawn(gameBoard, playerNumber, (x, y))

def play_pawn(gameBoard, playerNumber, coord):
    pos = get_board_pos_from_coordinate(coord)

    if pos :
        return put_pawn(gameBoard, playerNumber, pos)

    return pos

def rotate_frame(gameBoard, frameNumber, clockwise):
    if frameNumber > 0 and frameNumber <= CONFIG["FRAME_QUANTITY"]:
        SOUND_RESOURCES['ROTATEFRAME'].play()
        rotate_board(gameBoard, CONFIG['GAMEBOARD_SIZE'], frameNumber, CONFIG['GAME_SIZE'], clockwise)
        return True

    return False

def rotate_random_frame(gameBoard):
    time.sleep(CONFIG['AI_SLEEP_TIME'])
    frameNumber =  random.randrange(1, CONFIG['FRAME_QUANTITY'] + 1)
    clockwise =  random.randrange(0, 2) == 0
    rotate_frame(gameBoard, frameNumber, clockwise)

def play_frame(gameBoard, coord, clockwise):
    frameNumber = get_frame_number_from_coordinate(coord)

    if frameNumber:
        rotate_frame(gameBoard, frameNumber, clockwise)
        return True

    return False

def empty_square_remaining(gameBoard):
    for line in gameBoard:
        for value in line:
            if value == 0:
                return True

    return False

def is_empty(gameBoard):
    for line in gameBoard:
        for value in line:
            if value != 0:
                return False

    return True

def get_winner(gameBoard):
    player1 = alignment(gameBoard, CONFIG['GAMEBOARD_SIZE'], CONFIG['WIN_MIN_LENGTH_COND'], 1)
    player2 = alignment(gameBoard, CONFIG['GAMEBOARD_SIZE'], CONFIG['WIN_MIN_LENGTH_COND'], 2)

    if player1 and not player2:
        SOUND_RESOURCES['VICTORY'].play()
        return 1
    if player2 and not player1:
        SOUND_RESOURCES['VICTORY'].play()
        return 2
    if player1 and player2 or not empty_square_remaining(gameBoard):
        return 3

    return 0

def check_winner(gameBoard, window):
    winner = get_winner(gameBoard)

    if winner:
        display_game_end(winner, window)
        return False

    return True


def display_current_player(currentPlayer, surface):
    pygame.draw.rect(surface, CONFIG['BACKGROUND_COLOR'], (CONFIG['GAME_WIDTH'] + 20, 10, 80, 80))
    playerSurface = pygame.transform.scale(IMG_RESOURCES['PLAYER1'] if currentPlayer == 1 else IMG_RESOURCES['PLAYER2'], (60, 60))
    surface.blit(playerSurface, (CONFIG['GAME_WIDTH'] + 30, 20))

def switch_player(currentPlayer, surface):
    currentPlayer = 2 if currentPlayer == 1 else 1
    display_current_player(currentPlayer, surface)

    return currentPlayer

def blit_alpha(target, source, location, opacity):
        x = location[0]
        y = location[1]
        temp = pygame.Surface((source.get_width(), source.get_height())).convert()
        temp.blit(target, (-x, -y))
        temp.blit(source, (0, 0))
        temp.set_alpha(opacity)        
        target.blit(temp, location)

def display_action(action, surface):
    pygame.draw.rect(surface, CONFIG['BACKGROUND_COLOR'], (CONFIG['GAME_WIDTH'] + 10, 100, 100, 250))

    if action == AI_PLAYING:
        actionSurface = IMG_RESOURCES['COMPUTER']
    elif action == PUT_PAWN:
        actionSurface = IMG_RESOURCES['PUTPAWN']
    elif action == ROTATE_FRAME:
        actionSurface = IMG_RESOURCES['ROTATEFRAME']
    else:
        return

    surface.blit(actionSurface, (CONFIG['GAME_WIDTH'] + 10, 100))

def display_current_action(currentAction, currentPlayer, surface):
    if currentAction and CONFIG['PLAYERS'][currentPlayer] == COMPUTER:
        display_action(AI_PLAYING, surface)
    else:
        display_action(currentAction, surface)

def switch_current_action(currentAction, currentPlayer, surface):
    currentAction = ROTATE_FRAME if currentAction == PUT_PAWN else PUT_PAWN
    display_current_action(currentAction, currentPlayer, surface)

    return currentAction
    
def show_put_pawn_possibility(currentPlayer, pos, surface):#tmp transparency ...
    playerSurface = pygame.transform.scale(IMG_RESOURCES['PLAYER1'] if currentPlayer == 1 else IMG_RESOURCES['PLAYER2'], (CONFIG['SQUARE_TEXTURE_WIDTH'], CONFIG['SQUARE_TEXTURE_WIDTH']))
    coordinate = get_coordinate_from_board_pos(pos)
    blit_alpha(surface, playerSurface, (coordinate[0] + (CONFIG['SQUARE_WIDTH'] - CONFIG['SQUARE_TEXTURE_WIDTH'])/2, coordinate[1] + (CONFIG['SQUARE_WIDTH'] - CONFIG['SQUARE_TEXTURE_WIDTH'])/2), 180)

def show_play_frame_possibility(pos, surface):
    coordinate = get_coordinate_from_frame_number(get_frame_number_from_coordinate(pos))

    if coordinate:
        surface.blit(IMG_RESOURCES['FRAME_BORDER'], (coordinate[0], coordinate[1]))

def init_pygame():
    pygame.init()
    pygame.mixer.pre_init(44100, 16, 2, 4096)
    updateConfiguration()
    window = pygame.display.set_mode((CONFIG['WINDOW_WIDTH'], CONFIG['WINDOW_HEIGHT']))
    pygame.display.set_caption(CONFIG['WINDOW_TITLE'])
    loadRessources()

    return window

def display_game_end(winner, surface):
    if winner == 3:
        pygame.draw.rect(surface, (128, 128, 128), (CONFIG['GAME_WIDTH'] + 10, 10, 80, 80))
        pygame.draw.line(surface, (255, 0, 0), (CONFIG['GAME_WIDTH'] + 20, 20), (CONFIG['GAME_WIDTH'] + 80, 80), 5)
        pygame.draw.line(surface, (255, 0, 0), (CONFIG['GAME_WIDTH'] + 80, 20), (CONFIG['GAME_WIDTH'] + 20, 80), 5)
    else:
        display_current_player(winner, surface)
        surface.blit(IMG_RESOURCES['WINNER'], (CONFIG['GAME_WIDTH'], 0))

    display_action(NONE, surface)

def save_game(gameBoard, currentPlayer, currentAction):
    file = open(CONFIG['SAVE_FILE_NAME'], 'w')

    if file:
        #games rules
        file.write(str(CONFIG['FRAME_SIZE']) + ' ' + str(CONFIG['GAME_SIZE']) + ' ' + str(CONFIG['WIN_MIN_LENGTH_COND']) + '\n')

        #game state
        file.write(str(currentPlayer) + ' ' + str(currentAction) + '\n')

        for line in gameBoard:
            for value in line:
                file.write(str(value))
            file.write('\n')

        file.close()

def load_game():
    if os.path.isfile(CONFIG['SAVE_FILE_NAME']):
        file = open(CONFIG['SAVE_FILE_NAME'], 'r')

        if file:
            CONFIG['FRAME_SIZE'], CONFIG['GAME_SIZE'], CONFIG['WIN_MIN_LENGTH_COND'] = (int(value) for value in file.readline().split())
            updateConfiguration()
            currentPlayer, currentAction = (int(value) for value in file.readline().split())
            gameBoard = make_list(CONFIG['GAMEBOARD_SIZE'])

            i = 0
            for line in file:
                j = 0
                if i >= CONFIG['GAMEBOARD_SIZE']:
                    break
                for value in line:
                    if j >= CONFIG['GAMEBOARD_SIZE']:
                        break

                    gameBoard[i][j] = int(value)
                    j = j + 1

                i = i + 1

            file.close()
            os.remove(CONFIG['SAVE_FILE_NAME'])

            return gameBoard, currentPlayer, currentAction

    return False, 1, PUT_PAWN

def start_pentago():
    gameBoard, currentPlayer, currentAction = load_game()
    window = init_pygame()
    gameBoard = make_list(CONFIG['GAMEBOARD_SIZE']) if gameBoard == False else gameBoard
    
    inProgress = True
    inGame = True
    FPS = 10
    fpsClock = pygame.time.Clock()
    mousePos = (0, 0)
    gameSurface = pygame.Surface((CONFIG['GAME_WIDTH'], CONFIG['GAME_WIDTH']), pygame.SRCALPHA, 32).convert_alpha()
    pygame.draw.rect(window, CONFIG['BACKGROUND_COLOR'], (0, 0, CONFIG['WINDOW_WIDTH'], CONFIG['WINDOW_HEIGHT']))
    window.blit(IMG_RESOURCES['RESET'], (CONFIG['GAME_WIDTH'] + 10, 350))
    draw_board(gameBoard, gameSurface)
    display_current_player(currentPlayer, window)
    display_current_action(currentAction, currentPlayer, window)

    while inProgress:
        roundPlayed = False
        for event in pygame.event.get():
            if event.type == QUIT:
                inProgress = False
                if inGame and not is_empty(gameBoard):
                    save_game(gameBoard, currentPlayer, currentAction)
            elif event.type == MOUSEBUTTONUP:
                if inGame and not roundPlayed and CONFIG["PLAYERS"][currentPlayer] == HUMAN:
                    if currentAction == PUT_PAWN and event.button == 1:
                        if play_pawn(gameBoard, currentPlayer, event.pos):
                            draw_board(gameBoard, gameSurface)
                            currentAction =  switch_current_action(currentAction, currentPlayer, window)
                            roundPlayed = True
                    elif currentAction == ROTATE_FRAME and (event.button == 1 or event.button == 3):
                        clockwise = event.button == 3
                        if play_frame(gameBoard, event.pos, clockwise):
                            draw_board(gameBoard, gameSurface)
                            currentPlayer = switch_player(currentPlayer, window)
                            currentAction =  switch_current_action(currentAction, currentPlayer, window)
                            roundPlayed = True

                    inGame = check_winner(gameBoard, window)

                if  event.pos[0] > CONFIG['GAME_WIDTH'] + 10 and event.pos[0] < CONFIG['GAME_WIDTH'] + 110 and event.pos[1] > 350 and event.pos[1] < 442:
                    #on click on reset button
                    pygame.draw.rect(window, CONFIG['BACKGROUND_COLOR'], (0, 0, CONFIG['WINDOW_WIDTH'], CONFIG['WINDOW_HEIGHT']))
                    window.blit(IMG_RESOURCES['RESET'], (CONFIG['GAME_WIDTH'] + 10, 350))
                    gameBoard = make_list(CONFIG['GAMEBOARD_SIZE'])
                    draw_board(gameBoard, gameSurface)
                    currentPlayer = 1
                    display_current_player(currentPlayer, window)
                    currentAction = PUT_PAWN
                    display_current_action(currentAction, currentPlayer, window)
                    inGame = True
            elif event.type == MOUSEMOTION:
                mousePos = event.pos

        if(inGame and not roundPlayed and CONFIG["PLAYERS"][currentPlayer] == COMPUTER):
            if currentAction == PUT_PAWN:
                put_random_pawn(gameBoard, currentPlayer)
                draw_board(gameBoard, gameSurface)
                currentAction =  switch_current_action(currentAction, currentPlayer, window)
                roundPlayed = True
            elif currentAction == ROTATE_FRAME:
                rotate_random_frame(gameBoard)
                draw_board(gameBoard, gameSurface)
                currentPlayer = switch_player(currentPlayer, window)
                currentAction =  switch_current_action(currentAction, currentPlayer, window)
                roundPlayed = True

            inGame = check_winner(gameBoard, window)

        window.blit(gameSurface, (CONFIG['BOARD_POS_X'], CONFIG['BOARD_POS_Y']))

        if inGame and CONFIG["PLAYERS"][currentPlayer] == HUMAN:
            if currentAction == PUT_PAWN:
                pos = get_board_pos_from_coordinate(mousePos)
                if pos and gameBoard[pos[0]][pos[1]] == 0:
                    show_put_pawn_possibility(currentPlayer, pos, window)
            elif currentAction == ROTATE_FRAME:
                frameNumber = get_frame_number_from_coordinate(mousePos)
                if frameNumber:
                    show_play_frame_possibility(mousePos, window)

        pygame.display.update()
        fpsClock.tick(FPS)
        
    pygame.quit()

start_pentago()
