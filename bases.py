def make_list(n):
    return [[0] * n for i in range(n)]

def print_list(myList):
    for line in myList:
        for value in line:
            print(value, end=' ')
        print()

def get_frame_pos(frameNumber, gameSize):
	line = (frameNumber-1) // gameSize
	column = (frameNumber-1) % gameSize

	return (line, column) if line < gameSize else False

def get_frame_board_pos(frameNumber, frameSize, gameSize):
	pos = get_frame_pos(frameNumber, gameSize)

	return (pos[0] * frameSize, pos[1] * frameSize) if pos else False
    